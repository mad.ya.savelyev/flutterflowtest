import 'api_manager.dart';

Future<dynamic> testAPICallCall() => ApiManager.instance.makeApiCall(
      callName: 'Test API call',
      apiDomain: 'jsonplaceholder.typicode.com',
      apiEndpoint: 'users',
      callType: ApiCallType.GET,
      headers: {},
      params: {},
      returnResponse: true,
    );
